<?php

namespace App\Providers;

use App\Services\CurrencyService;
use App\Services\CurrencyServiceInterface;
use App\Services\MoneyService;
use App\Services\MoneyServiceInterface;
use App\Services\UserService;
use App\Services\UserServiceInterface;
use App\Services\WalletService;
use App\Services\WalletServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CurrencyServiceInterface::class, CurrencyService::class);
        $this->app->singleton(UserServiceInterface::class, UserService::class);
        $this->app->singleton(WalletServiceInterface::class, WalletService::class);
        $this->app->singleton(MoneyServiceInterface::class, MoneyService::class);
    }
}
