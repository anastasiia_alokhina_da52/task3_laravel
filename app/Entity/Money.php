<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;

class Money extends Pivot
{
    use SoftDeletes;

    const DELETED_AT = 'deleted';

    protected $table = 'money';

    protected $dates = ['deleted'];

    public $timestamps = false;

    protected $fillable = ['currency_id', 'wallet_id', 'amount'];
}
