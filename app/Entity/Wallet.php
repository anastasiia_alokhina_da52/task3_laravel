<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    const DELETED_AT = 'deleted';

    protected $table = 'wallet';

    protected $dates = ['deleted'];

    protected $fillable = ['user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currencies()
    {
        return $this->belongsToMany(Currency::class, 'money')
            ->withPivot('amount', 'deleted')
            ->as('money');
    }
}
