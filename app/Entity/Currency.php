<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currency';

    public $timestamps = false;

    protected $fillable = ['name'];

    public function wallets()
    {
        return $this->belongsToMany(Wallet::class, 'money')
            ->withPivot('amount', 'deleted')
            ->as('money');
    }
}
