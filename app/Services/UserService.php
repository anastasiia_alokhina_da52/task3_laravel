<?php


namespace App\Services;


use App\Entity\User;
use App\Entity\Wallet;
use App\Requests\SaveUserRequest;
use Illuminate\Support\Collection;

class UserService implements UserServiceInterface
{
    public function findAll(): Collection
    {
        return User::all();
    }

    public function findById(int $id): ?User
    {
        return User::find($id);
    }

    public function save(SaveUserRequest $request): User
    {
        if ($request->getId()) {
            $user = $this->findById($request->getId());
        } else {
            $user = new User();
        }
        $user->name = $request->getName();
        $user->email = $request->getEmail();
        $user->save();
        return $user;
    }

    public function delete(int $id): void
    {
        User::destroy($id);
        Wallet::where('user_id', $id)->delete();
    }
}