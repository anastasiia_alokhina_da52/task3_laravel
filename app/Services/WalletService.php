<?php


namespace App\Services;


use App\Entity\Wallet;
use App\Requests\CreateWalletRequest;
use Illuminate\Support\Collection;

class WalletService implements WalletServiceInterface
{
    public function findByUser(int $userId): ?Wallet
    {
        return Wallet::where('user_id', $userId)->first();
    }

    public function create(CreateWalletRequest $request): Wallet
    {
        $wallet = new Wallet();
        if (Wallet::where('user_id', $request->getUserId())->count() === 0) {
            $wallet->user_id = $request->getUserId();
            $wallet->save();
            return $wallet;
        } else {
            throw new \LogicException("Wallet for this user already exists");
        }
    }

    public function findCurrencies(int $walletId): Collection
    {
        return Wallet::find($walletId)->currencies;
    }
}