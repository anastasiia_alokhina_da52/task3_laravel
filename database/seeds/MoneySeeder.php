<?php

use Illuminate\Database\Seeder;
use App\Entity\Money;

class MoneySeeder extends Seeder
{
    public function run()
    {
        factory(Money::class, 50)->create();
    }
}