<?php

use Illuminate\Database\Seeder;
use App\Entity\Currency;

class CurrencySeeder extends Seeder
{
    public function run()
    {
        factory(Currency::class, 10)->create();
    }
}