<?php

use Illuminate\Database\Seeder;
use App\Entity\Wallet;

class WalletSeeder extends Seeder
{
    public function run()
    {
        factory(Wallet::class, 10)->create();
    }
}