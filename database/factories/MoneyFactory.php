<?php

use Faker\Generator as Faker;
use \App\Entity\Currency;
use App\Entity\Wallet;
use App\Entity\Money;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Money::class, function(Faker $faker) {
    return [
        'wallet_id' => Wallet::all()->random()->id,
        'currency_id' => Currency::all()->random()->id,
        'amount' => $faker->randomFloat(3, 0, 100000)
    ];
});
